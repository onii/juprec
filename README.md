# A bash script to record live jupiterbroadcasting audio streams

## There's a newer project that replaces this. [Check it out!](https://gitlab.com/onii/internet-radio)

Requires `wget` and `at`.

Typical cron might look something like:

    35 13 * * Sun /home/username/bin/juprec.sh LAS >/dev/null 2>&1
    35 15 * * Tue /home/username/bin/juprec.sh LUP >/dev/null 2>&1
