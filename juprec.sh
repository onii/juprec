#!/bin/bash
today=`date +%-m-%-d-%y`
# Today's date (m-d-y)
mtoday=`date +%Y-%m-%d`
# Metadata date
stime=`date +%s`
# Epoch script start time
retries=0
cd /mnt/Sandisk_32GB_USB/Jupiter/
rec () {
    curl http://jblive.fm >> $1_"$today".mp3 &
    pget=$!
    let "ektime = $stime + ($2*60)"
    # Epoch kill time = Epoch start time + dur (m)
    ktime=`date -d @$ektime +%H:%M`
    # Convert Epoch to %H:%M for at command
    at $ktime <<< "kill $pget"
    wait $pget
    # Wait until wget exits
    if [ $? != 143 ]; then
        # If this happens, we got a bad exit code
        ((retries++))
        if [ $retries == 60 ]; then
            echo "This is pointless. I'm giving up."
            exit 1
        else
            echo "Stream broke - trying again. Attempt: $retries"
            sleep 30
            rec $1 $2
        fi
    fi
#    mid3v2 -a "Jupiter Broadcasting" -A "Jupiter Broadcasting" -t "$1 $today" \
#    -y $mtoday $1_"$today".mp3
    # Add metadata
#    avconv -i $1_"$today".mp3 -i /home/username/Pictures/Jup-Cover.jpg \
#    -map 0 -map 1 -acodec copy $1_"$today"-image.mp3
    # Add cover artwork
#    file1=$(stat -c%s $1_"$today".mp3)
#    file2=$(stat -c%s $1_"$today"-image.mp3)
#    if [ "$file1" -lt "$file2" ]; then
#        mv $1_"$today"-image.mp3 $1_"$today".mp3
#    else
#        rm $1_"$today"-image.mp3
        # Something went wrong with avconv
#    fi
    exit 0
}

if [ $1 == "LAS" ]; then
    rec "LAS" "240"
elif [ $1 == "LUP" ]; then
    rec "LUP" "180"
elif [ $1 == "UE" ]; then
    rec "UE" "200"
elif [ $1 == "TTT" ]; then
    rec "TTT" "120"
fi
